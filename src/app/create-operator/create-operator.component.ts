import { Component, OnInit } from '@angular/core';
import { OperatorService } from '../operator.service';
import { Operator } from '../operator';
import { NgModel } from '@angular/forms'; 

@Component({
  selector: 'app-create-operator',
  templateUrl: './create-operator.component.html',
  styleUrls: ['./create-operator.component.css']
})
export class CreateOperatorComponent implements OnInit {

  operators: Operator[];
  operatorName:String="";

  constructor(public operatorService:OperatorService) { }  

  ngOnInit() {
    this.getOperators();
  }

  public static canCreateFlag:Boolean = true;
  public canCreate():Boolean{
    return CreateOperatorComponent.canCreateFlag && this.operatorName.length > 0;
  } 

  private getOperators(): void {
    this.operatorService.getOperators().subscribe(operator => this.operators = operator);
  }

  public create():void {
    this.operatorName = this.operatorName.trim();
    if (!this.operatorName) { return; }

    CreateOperatorComponent.canCreateFlag = false;
    this.operatorService.addOperator(new Operator(this.operatorService.operatorId(), this.operatorName), this.createCallback);
  }

  public createCallback(success:boolean, data:any = null):void
  {
    console.log("createCallback: ", success, data);
    CreateOperatorComponent.canCreateFlag = true;
  }

}
