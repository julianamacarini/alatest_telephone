import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { CreateOperatorComponent } from './create-operator.component';
import { FormsModule } from '@angular/forms';
import { OperatorService } from '../operator.service';

describe('CreateOperatorComponent', () => {
  
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateOperatorComponent ],
      imports: [
        FormsModule
      ],
      providers: [OperatorService] 
    }).compileComponents();
  }));

  let component: CreateOperatorComponent;
  let fixture: ComponentFixture<CreateOperatorComponent>;
  beforeEach(async(() => {
    fixture = TestBed.createComponent(CreateOperatorComponent);
    component = fixture.componentInstance;
  }));

  // test 1
  it("dependencies", async(() => {
    console.log("dependencies:", component);
    expect(component).toBeTruthy();
  }));

  // test 2
  it("add operator", async(() => {    
    setTimeout(()=>{    
      // populate list
      fixture.componentInstance.operatorName = "op.Test0"; 
      fixture.nativeElement.querySelector("button").click();
      fixture.componentInstance.operatorName = "op.Test1";
      fixture.nativeElement.querySelector("button").click();
      // test call
      spyOn(fixture.componentInstance, "create");
      fixture.componentInstance.operatorName = "temp";
      fixture.nativeElement.querySelector("button").click();
      let result = fixture.componentInstance.create;
      console.log("add operator:", result);
      expect(result).toHaveBeenCalled();
    },0);    
  }));


});
