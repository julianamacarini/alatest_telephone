import { QueryList } from "@angular/core/src/linker/query_list";
import { AreaCode } from "./area-code";



export class Operator {
    
    public id: number;
    public name: String;
    public areaCodes: Array<AreaCode>;
    
    
    constructor(id:number, name:String, prices:Array<AreaCode>=null) {
        this.id = id;
        this.name = name;
        this.areaCodes = prices ? prices : new Array<AreaCode>();
    } 
    
    private hasAreaCode(areaCode:String):AreaCode {
        return this.areaCodes.find(x => x.code == areaCode);
    }
    
    public registerAreaCodePrice(areaCode:String, price:Number, callback:Function):void {
        let item:AreaCode = this.hasAreaCode(areaCode);
        if(!item) {            
            this.areaCodes.push(new AreaCode(areaCode, price));
            if(callback) callback(true, this.areaCodes);
        }
        else {
            item.price = price;
            if(callback) callback(false, this.areaCodes);
        }

        
    }
    
    public removeAreaCode(areaCode:String, callback:Function):void {
        let item:AreaCode = this.hasAreaCode(areaCode);
        if(item) {
            let index = this.areaCodes.indexOf(item);
            this.areaCodes.splice(index, 1);             
            if(callback) callback(true, this.areaCodes);
        }         
        else if(callback) callback(false, this.areaCodes);
    }
    
    

    public getPriceForLongestAreaCode(telephone:String):Array<Number> {
        let longestAreaCodeSize:Number = 0;
        let price:Number = -1;
        let flag : boolean = false;
        for (let item of this.areaCodes) {
            let indexx  = telephone.indexOf(item.code.toString());
            if(indexx == 0) {
                if(item.code.length > longestAreaCodeSize) {
                    longestAreaCodeSize = item.code.length;
                    price = item.price;
                }
            }
        }
        return [longestAreaCodeSize, price];
    } 
    
}