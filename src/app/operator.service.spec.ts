import { async, TestBed, inject } from '@angular/core/testing';
import { Operator } from './operator';
import { AreaCode } from './area-code';
import { OperatorService } from './operator.service';

describe('OperatorService', () => {  
  
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      providers: [OperatorService]
    });
  }));
  
  // test 1
  it("dependencies", inject([OperatorService], (service: OperatorService) => {
    console.log("dependencies:", service);
    expect(service).toBeTruthy();
  }));
  
  
  // // test 2
  // it("add Operator", inject([OperatorService], (service: OperatorService) => {
    
  //   let operator1:Operator = null;
  //   service.addOperator(operator1 = new Operator(3, "op.direct.by.service.0", [ new AreaCode("016", 0.97)]));
  //   let operators:Operator[];
  //   service.getOperators().subscribe(operator => operators = operator, null, ()=>{
      
  //     console.log(operators, operator1);
      
  //   });
    
  }));
  
  
});


