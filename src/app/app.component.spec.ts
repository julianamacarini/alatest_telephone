import { TestBed, async, ComponentFixture } from '@angular/core/testing';
import { AppComponent } from './app.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { OperatorListComponent } from './operator-list/operator-list.component';
import { CreateOperatorComponent } from './create-operator/create-operator.component';
import { OperatorComponent } from './operator/operator.component';
import { OperatorService } from './operator.service';
import { STRING_TYPE } from '@angular/compiler/src/output/output_ast';

describe('AppComponent', () => {
  
  // module configuration
  beforeEach(async(() => {
    TestBed.configureTestingModule({   
      declarations: [
        AppComponent,
        OperatorListComponent,
        CreateOperatorComponent,
        OperatorComponent 
      ],
      imports: [
        FormsModule
      ],
      providers: [OperatorService]  
    }).compileComponents();  
  }));
  
  // initialize app / fixture
  let app: AppComponent;
  let fixture: ComponentFixture<AppComponent>;
  beforeEach(async(() => {
    fixture = TestBed.createComponent(AppComponent);
    app = fixture.componentInstance;
  }));
  
  // test 1
  it("dependencies", async(() => { 
    console.log("dependencies:", app);
    expect(app).toBeTruthy();
  }));  
  
  // test 2
  it("input only numbers", async(() => {
    setTimeout(()=>{   
      fixture.componentInstance.phone = "123abc!@#,. "; 
      fixture.detectChanges();       
      let inputText:string = fixture.nativeElement.querySelector("input").value;
      let result = inputText.match(/^\d+$/);
      console.log("input only numbers:", result);
      expect(result).toBeFalsy(); 
    },0);
  }));  
  
  // test 3
  it("search click callback", async(() => {
    setTimeout(()=>{
      fixture.componentInstance.phone = "123abc!@#,. "; 
      fixture.detectChanges();       
      spyOn(fixture.componentInstance, "searchCallback"); 
      fixture.nativeElement.querySelector("button").click();
      let result = fixture.componentInstance.searchCallback;
      console.log("search click callback:", result);
      expect(result).toHaveBeenCalled();  
    },0);
  })); 
  
});
