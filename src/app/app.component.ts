import { Component, Input } from '@angular/core';
import { OperatorService } from './operator.service';
import { NgModel } from '@angular/forms';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent {
  
  constructor(private operatorService:OperatorService ) {}
  
  public phone:String = "";  
  
  public static canSearchFlag:Boolean = true;
  public canSearch():Boolean{
    return AppComponent.canSearchFlag && this.getPhoneValue().length > 0;
  } 
  
  public getPhoneValue():String {
    return this.operatorService.inputFilterNumbers(this.phone);
  }
  
  private search(): void { 
    AppComponent.canSearchFlag = false;
    this.operatorService.getLowestPrice(this.getPhoneValue(), this.searchCallback);
  }
  
  public searchCallback(success:Boolean, lowestPrice:Number):void {
    console.log("searchCallback: ", success, lowestPrice);
    AppComponent.canSearchFlag = true;

    window.alert("found: " +success + '\nprice: ' + lowestPrice);
  }
  
  
  
  
  
  
}
