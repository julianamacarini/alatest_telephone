import { Component, Input } from '@angular/core';
import { Operator } from '../operator';
import { NgModel } from '@angular/forms';
import { CurrencyMaskModule } from "ng2-currency-mask";
import { OperatorService } from '../operator.service';


@Component({
  selector: 'app-operator', 
  templateUrl: './operator.component.html',
  styleUrls: ['./operator.component.css']
}) 

export class OperatorComponent {
  
  @Input() operator:Operator;

  areaCode:string;
  price:string;
      
  constructor(public operatorService:OperatorService) { } 
  
  public removeOperatorCallback(success:boolean, data:any) :void {
    console.log("removeOperatorCallback: ", success, data);
  }

  public registerAreaCodePriceCallback(success:boolean, data:any) :void {
    console.log("registerAreaCodePriceCallback: ", success, data);
  }

  public removeAreaCodeCallback(success:boolean, data:any) :void {
    console.log("removeAreaCodeCallback: ", success, data);    
  }
  
}
