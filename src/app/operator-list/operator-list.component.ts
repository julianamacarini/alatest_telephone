import { Component, OnInit } from '@angular/core';
import { Operator } from '../operator';
import { OperatorService } from '../operator.service';

@Component({ 
  selector: 'app-operator-list',
  templateUrl: './operator-list.component.html',
  styleUrls: ['./operator-list.component.css']
})
export class OperatorListComponent implements OnInit {
  
  operators: Operator[];  
  
  constructor(private operatorService: OperatorService) {}
  
  ngOnInit() {
    this.getOperators();
  }
  
  private getOperators(): void {
    this.operatorService.getOperators().subscribe(operator => this.operators = operator);
  }
  
  
  
}
