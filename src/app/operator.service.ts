import { Injectable } from '@angular/core';
import { Operator } from './operator';
import { OPERATORS } from './mock-operators';
import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';

@Injectable()
export class OperatorService {
  
  constructor() { }  
  
  public getOperators(): Observable<Operator[]> {
    return of(OPERATORS);
  }  
  
  public addOperator (operator: Operator, callback:Function): void {   
    let operators:Operator[];    
    this.getOperators().subscribe( 
      operator => operators = operator, 
      error=>{
        if(callback) callback(false, error);
      },
      ()=>{
        let item = operators.find(x => x.name == operator.name);
        if(!item){
          operators.push(operator);
          if(callback) callback(true, operators);
        } 
        else if(callback) callback(false, operators);
      }
    );
  }
  
  
  public removeOperator(operatorId:number, callback:Function):void {
    let operators:Operator[];
    this.getOperators().subscribe( 
      operator => operators = operator, 
      error=>{
        if(callback) callback(false, error);
      }, 
      ()=>{
        let item = operators.find(x => x.id == operatorId);
        if(item) {
          let index = operators.indexOf(item);
          operators.splice(index, 1);    
          if(callback) callback(true, operators);    
        }
      });
    }
    
    public getLowestPrice (telephone:String, callback:Function):void {    
      let operators:Operator[];
      this.getOperators().subscribe( 
        operator => operators = operator, 
        error=>{
          if(callback) callback(false, error);
        },
        ()=>{          
          let map:Map<Number, Number> = new Map<Number, Number>();
          let lowestPrice:Number = null; 
          for (let item of operators) {
            let operatorPrice = item.getPriceForLongestAreaCode(telephone);
            if(operatorPrice[1]>=0 && (lowestPrice==null || lowestPrice > operatorPrice[1])) {
              lowestPrice = operatorPrice[1];
              map.set(operatorPrice[0], lowestPrice);
            }
          }                 
          let longestKey:Number = 0;
          map.forEach(function(value:Number, key:Number, map:Map<Number, Number>):void{
            if(key > longestKey) longestKey = key;
          });          
          let result = map.get(longestKey);

          if(callback) callback(Boolean(result), result);  
                       
        });
      }
      
      
      public inputFilterNumbers(value:String):String
      {
        return value.replace(/\D/g,'');
      }
      
      private operatorIdCounter:number = 0;
      public operatorId():number{
        return this.operatorIdCounter++;
      }
      
      
      
      
    }
    