import { BrowserModule } from '@angular/platform-browser';
import { NgModule} from '@angular/core';

import { AppComponent } from './app.component';
import { OperatorListComponent } from './operator-list/operator-list.component'; 
import { CreateOperatorComponent } from './create-operator/create-operator.component';
import { OperatorService } from './operator.service';
import { OperatorComponent } from './operator/operator.component';
import { CurrencyMaskModule } from "ng2-currency-mask";
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    AppComponent,
    OperatorListComponent,
    CreateOperatorComponent,
    OperatorComponent    
  ],
  imports: [
    BrowserModule,
    CurrencyMaskModule,
    FormsModule
  ],
  providers: [OperatorService],
  bootstrap: [AppComponent]
})
export class AppModule { }
