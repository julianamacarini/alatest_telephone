
export class AreaCode {    
  code: String;
  price: Number;  

  constructor(code:String, price:Number) {
      this.code = code;
      this.price = price;
  }
}